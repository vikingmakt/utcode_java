package br.com.vikingmakt.utcode;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

import br.com.vikingmakt.utcode.marshalers.UTCodeBooleanMarshaler;
import br.com.vikingmakt.utcode.marshalers.UTCodeIntegerMarshaler;
import br.com.vikingmakt.utcode.marshalers.UTCodeDoubleMarshaler;
import br.com.vikingmakt.utcode.marshalers.UTCodeStringMarshaler;

public class UTCode {

  private HashMap<Class<?>, UTCodeMarshaler> _encoders = new HashMap<>();
  private HashMap<Character, UTCodeMarshaler> _decoders = new HashMap<>();

  public UTCode() {
    registerDefaultTypes();
  }

  public void registerType(Class<?> cls, Character repr, UTCodeMarshaler marshaler) {
    _encoders.put(cls, marshaler);
    _decoders.put(repr, marshaler);
  }

  public String encode(Object value) throws UTCodeException {
    try {
      Writer w = new StringWriter();
      encode(value, w);
      return w.toString();
    } catch (IOException e) {
      return null;
    }
  }

  public void encode(Object value, Writer w) throws IOException, UTCodeException {
    w.write("ut:");
    encodeType(value, w);
  }

  public void encodeType(Object value, Writer w) throws IOException, UTCodeException {
    if (value == null) {
      w.write("n:e");
    } else if (!_encoders.containsKey(value.getClass())) {
      throw new UTCodeException(String.format("no marshaler registered for '%s'", value.getClass().getName()));
    } else {
      _encoders.get(value.getClass()).encode(value, this, w);
    }
  }

  public Object decode(String str) throws UTCodeException {
    try {
      return decode(new StringReader(str));
    } catch (IOException e) {
      return null;
    }
  }

  public Object decode(Reader r) throws IOException, UTCodeException {
    char[] begin = new char[3];
    r.read(begin, 0, 3);
    if (!(new String(begin)).equals("ut:")) {
      throw new UTCodeException("invalid utcode");
    }
    return decodeType(r);
  }

  public Object decodeType(Reader r) throws IOException, UTCodeException {
    char t = (char) r.read();
    if (!_decoders.containsKey(t)) {
      throw new UTCodeException(String.format("invalid type %s", String.valueOf((char) t)));
    }
    return _decoders.get(t).decode(r, this);
  }

  private void registerDefaultTypes() {
    registerType(Boolean.class, 'b', new UTCodeBooleanMarshaler());
    registerType(Integer.class, 'i', new UTCodeIntegerMarshaler());
    registerType(Float.class, 'f', new UTCodeDoubleMarshaler());
    registerType(Double.class, 'f', new UTCodeDoubleMarshaler());
    registerType(String.class, 's', new UTCodeStringMarshaler());
  }

  public static void main(String[] args) {
    UTCode utcode = new UTCode();
    try {
      System.out.println(utcode.decode(utcode.encode("hello darkness")));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
