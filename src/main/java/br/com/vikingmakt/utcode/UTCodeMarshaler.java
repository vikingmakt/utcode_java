package br.com.vikingmakt.utcode;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public interface UTCodeMarshaler<T> {

  void encode(T v, UTCode utcode, Writer w) throws IOException;
  T decode(Reader r, UTCode utcode) throws IOException, UTCodeException;
}