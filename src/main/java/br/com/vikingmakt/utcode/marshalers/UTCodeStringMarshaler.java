package br.com.vikingmakt.utcode.marshalers;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import br.com.vikingmakt.utcode.UTCode;
import br.com.vikingmakt.utcode.UTCodeException;
import br.com.vikingmakt.utcode.UTCodeMarshaler;

public class UTCodeStringMarshaler implements UTCodeMarshaler<String> {

  @Override
  public void encode(String s, UTCode utcode, Writer w) throws IOException {
    w.write(String.format(getFormatString(s), s.length(), s));
  }

  @Override
  public String decode(Reader r, UTCode utcode) throws IOException, UTCodeException {
    StringBuilder b = new StringBuilder();
    int c;
    while ((c = r.read()) != ':') {
      b.append((char) c);
    }
    int length = Integer.valueOf(b.toString());
    String result = decodeLength(r, length);
    if (hasUnicode(result)) {
      //result = decodeUnicode(result);
    }
    return result;
  }

  private String decodeLength(Reader r, int length) throws IOException {
    StringBuilder b = new StringBuilder();
    for (int i = 0; i < length; i++) {
      b.append((char) r.read());
    }
    return b.toString();
  }

  private boolean hasUnicode(String str) {
    for (int i = 0; i < str.length(); i++) {
      if (str.charAt(i) >= 128) {
        return true;
      }
    }
    return false;
  }

  private String getFormatString(String str) {
    if (hasUnicode(str)) {
      return "u%d:%s";
    }
    return "s%d:%s";
  }
}
