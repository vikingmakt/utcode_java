package br.com.vikingmakt.utcode.marshalers;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Locale;

import br.com.vikingmakt.utcode.UTCode;
import br.com.vikingmakt.utcode.UTCodeException;
import br.com.vikingmakt.utcode.UTCodeMarshaler;

public class UTCodeDoubleMarshaler implements UTCodeMarshaler<Double> {

  @Override
  public void encode(Double d, UTCode utcode, Writer w) throws IOException {
    w.write(String.format(Locale.US, "f:%fe", d));
  }

  @Override
  public Double decode(Reader r, UTCode utcode) throws IOException, UTCodeException {
    StringBuilder builder = new StringBuilder();
    char c = (char) r.read();
    while ((c = (char) r.read()) != 'e') {
      builder.append(c);
    }
    return Double.valueOf(builder.toString());
  }
}
