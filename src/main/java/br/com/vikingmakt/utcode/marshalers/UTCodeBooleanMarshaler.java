package br.com.vikingmakt.utcode.marshalers;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import br.com.vikingmakt.utcode.UTCode;
import br.com.vikingmakt.utcode.UTCodeException;
import br.com.vikingmakt.utcode.UTCodeMarshaler;

public class UTCodeBooleanMarshaler implements UTCodeMarshaler<Boolean> {

  @Override
  public void encode(Boolean b, UTCode utcode, Writer w) throws IOException {
    w.write(String.format("b:%d", b ? 1 : 0));
  }

  @Override
  public Boolean decode(Reader r, UTCode utcode) throws IOException, UTCodeException {
    return r.read() == ':' && r.read() == '1';
  }
}
