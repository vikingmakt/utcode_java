package br.com.vikingmakt.utcode.marshalers;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import br.com.vikingmakt.utcode.UTCode;
import br.com.vikingmakt.utcode.UTCodeException;
import br.com.vikingmakt.utcode.UTCodeMarshaler;

public class UTCodeIntegerMarshaler implements UTCodeMarshaler<Integer> {

  @Override
  public void encode(Integer i, UTCode utcode, Writer w) throws IOException {
    w.write(String.format("i:%de", i));
  }

  @Override
  public Integer decode(Reader r, UTCode utcode) throws IOException, UTCodeException {
    StringBuilder builder = new StringBuilder();
    char c = (char) r.read();
    while ((c = (char) r.read()) != 'e') {
      builder.append(c);
    }
    return Integer.valueOf(builder.toString());
  }
}
