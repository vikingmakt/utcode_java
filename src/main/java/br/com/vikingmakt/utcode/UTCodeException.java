package br.com.vikingmakt.utcode;

public class UTCodeException extends Exception {

  public UTCodeException(String msg) {
    super(msg);
  }
}
